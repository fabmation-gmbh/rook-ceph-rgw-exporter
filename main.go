package main

import (
	"context"
	"log"
	"net/http"
	"os"

	"github.com/prometheus/client_golang/prometheus/promhttp"
	"go.opentelemetry.io/otel"
	"go.opentelemetry.io/otel/exporters/prometheus"
	api "go.opentelemetry.io/otel/metric"
	"go.opentelemetry.io/otel/sdk/metric"
	semconv "go.opentelemetry.io/otel/semconv/v1.21.0"

	"gitlab.com/fabmation-gmbh/rook-ceph-rgw-exporter/pkg/radosgw"
)

func loadConfig() *radosgw.Config {
	cfg := &radosgw.Config{
		ServerURL:       os.Getenv("SERVER_URL"),
		AdminPath:       os.Getenv("ADMIN_PATH"),
		AccessKeyID:     os.Getenv("ACCESS_KEY"),
		SecretAccessKey: os.Getenv("SECRET_ACCESS_KEY"),
	}

	if cfg.ServerURL == "" {
		log.Fatal("SERVER_URL cannot be empty!")
	}

	if cfg.AdminPath == "" {
		log.Fatal("ADMIN_PATH cannot be empty!")
	}

	if cfg.AccessKeyID == "" {
		log.Fatal("ACCESS_KEY cannot be empty!")
	}

	if cfg.SecretAccessKey == "" {
		log.Fatal("SECRET_ACCESS_KEY cannot be empty!")
	}

	return cfg

}

func main() {
	cli := radosgw.NewClient(loadConfig())

	exporter, err := prometheus.New()
	if err != nil {
		log.Fatal(err)
	}

	provider := metric.NewMeterProvider(metric.WithReader(exporter))
	meter := provider.Meter("rgw_exporter")

	col := newCollector(cli, meter)
	col.init(meter)

	log.Println("Start listening on :9101")

	http.Handle("/metrics", promhttp.Handler())
	http.Handle("/", livenessHandler{})

	log.Fatal(http.ListenAndServe(":9101", nil))
}

type livenessHandler struct{}

func (livenessHandler) ServeHTTP(resp http.ResponseWriter, _ *http.Request) {
	resp.Write([]byte("ok"))
}

type rgwCollector struct {
	cli *radosgw.Client

	actualSizeBytes api.Int64ObservableGauge
	objectsNumber   api.Int64ObservableGauge
}

func newCollector(cli *radosgw.Client, meter api.Meter) *rgwCollector {
	rc := &rgwCollector{
		cli: cli,
	}

	var err error

	rc.actualSizeBytes, err = meter.Int64ObservableGauge("ceph_rgw_bucket_size_actual_bytes",
		api.WithDescription("The actual size of the bucket in bytes"),
		api.WithUnit("By"),
	)
	handleError(err)

	rc.objectsNumber, err = meter.Int64ObservableGauge("ceph_rgw_bucket_number_objects",
		api.WithDescription("The number of objects stored in the bucket"),
		api.WithUnit("{objects}"),
	)
	handleError(err)

	return rc
}

func (r *rgwCollector) init(meter api.Meter) {
	meter.RegisterCallback(r.collect,
		r.actualSizeBytes, r.objectsNumber,
	)
}

func (r *rgwCollector) collect(ctx context.Context, o api.Observer) error {
	stats, err := r.cli.BucketStats(ctx, "", "")
	if err != nil {
		return err
	}

	for _, stat := range stats {
		if stat.Usage.RGWMain == nil {
			continue
		}

		attr := api.WithAttributes(semconv.AWSS3Bucket(stat.Bucket))
		usage := stat.Usage.RGWMain

		o.ObserveInt64(r.actualSizeBytes, int64(usage.Size), attr)
		o.ObserveInt64(r.objectsNumber, int64(usage.NumObjects), attr)
	}

	return nil
}

func handleError(err error) {
	if err == nil {
		return
	}

	otel.Handle(err)
}
