# Rook Ceph RGW Metric Exporter

This _very simple_ prometheus exporter provides additional metrics from the RadosGW.

## Getting started

A user can be created by executing the following commands in the _rook ceph toolbox_ pod:
```bash
~$ radosgw-admin user create --uid=rgw-exporter --display-name="Ceph RGW Exporter
~$ radosgw-admin caps add --uid=rgw-exporter --caps="users=read;mdlog=read;bilog=read;buckets=read;metadata=read;usage=read;zone=read"
```

After the user has been created, the exporter can be deployed with Helm.

**NOTE:** Do not forget to create a _Service Monitor object_ if you're using _prometheus operator_.

## Metrics

### `ceph_rgw_bucket_size_actual_bytes`

* Type: Gauge

Actual size of the bucket in bytes.

### `ceph_rgw_bucket_number_objects`

* Type: Gauge

Actual number of objects store in the bucket.
