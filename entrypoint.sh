#!/bin/bash

set -euxo pipefail

err() {
  echo "[FATAL] $@"
  exit 1
}

exec $@
