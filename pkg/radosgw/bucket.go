package radosgw

// BucketStatsResponse represents the bucket stats repsonse.
type BucketStatsResponse struct {
	Bucket      string       `json:"bucket"`
	Pool        string       `json:"pool"`
	IndexPool   string       `json:"index_pool"`
	ID          string       `json:"id"`
	Marker      string       `json:"marker"`
	Owner       string       `json:"owner"`
	Ver         string       `json:"ver"`
	MasterVer   string       `json:"master_ver"`
	MaxMarker   string       `json:"max_marker"`
	Usage       BucketUsage  `json:"usage"`
	BucketQuota *BucketQuota `json:"bucket_quota"`

	//Mtime       RadosTime    `json:"mtime"`
}

// BucketQuota is the bucket quota.
type BucketQuota struct {
	Enabled    bool  `json:"enabled"`
	MaxSizeKb  int64 `json:"max_size_kb"`
	MaxObjects int64 `json:"max_objects"`
}

// BucketUsage - Bucket usage entries
type BucketUsage struct {
	RGWNone      *BucketUsageEntry `json:"rgw.none,omitempty"`
	RGWMain      *BucketUsageEntry `json:"rgw.main,omitempty"`
	RGWShadow    *BucketUsageEntry `json:"rgw.shadow,omitempty"`
	RGWMultiMeta *BucketUsageEntry `json:"rgw.multimeta,omitempty"`
}

// BucketUsageEntry - entry for each bucket usage bit.
type BucketUsageEntry struct {
	SizeKb       uint64 `json:"size_kb"`
	SizeKbActual uint64 `json:"size_kb_actual"`
	NumObjects   uint64 `json:"num_objects"`

	// Size is the bucket size in bytes.
	Size int64 `json:"size"`
	// SizeActual is the actual/ raw size in bytes.
	// This metric tracks additional storage usage used for stuff
	// like the index, ...
	SizeActual   int64 `json:"size_actual"`
	SizeUtilized int64 `json:"size_utilized"`
}

type bucketStatsRequest struct {
	Bucket string `url:"bucket,omitempty"`
	UID    string `url:"uid,omitempty"`
	Stats  bool   `url:"stats,omitempty"`
}
