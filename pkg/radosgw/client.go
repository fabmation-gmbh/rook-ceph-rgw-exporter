package radosgw

import (
	"bytes"
	"context"
	"crypto/hmac"
	"crypto/md5"
	"crypto/sha1"
	"encoding/base64"
	"net/url"
	"sort"
	"strings"
	"time"

	"github.com/imroc/req/v3"
)

type Config struct {
	ServerURL       string
	AdminPath       string
	AccessKeyID     string
	SecretAccessKey string
}

type Client struct {
	cfg *Config
	cli *req.Client
}

func NewClient(cfg *Config) *Client {
	cli := &Client{
		cfg: cfg,
	}

	cli.cli = req.NewClient().
		SetBaseURL(cfg.ServerURL + cfg.AdminPath).
		OnBeforeRequest(cli.authMiddleware)

	return cli
}

func (c *Client) getTime() string {
	const timeFormatS3 = time.RFC1123Z

	return time.Now().UTC().Format(timeFormatS3)
}

func (c *Client) authMiddleware(client *req.Client, req *req.Request) error {
	// prepare
	req.Headers.Set("Date", c.getTime())

	data := c.authS3Sign(req)

	var tmp bytes.Buffer
	tmp.WriteString("AWS ")
	tmp.WriteString(c.cfg.AccessKeyID)
	tmp.WriteString(":")
	tmp.WriteString(base64.StdEncoding.EncodeToString(hmacSHA1([]byte(c.cfg.SecretAccessKey), data)))

	req.Headers.Set("Authorization", tmp.String())

	return nil
}

func (c *Client) authS3Sign(req *req.Request) []byte {
	var buf bytes.Buffer

	buf.WriteString(req.Method)
	buf.WriteByte('\n')

	if x := req.Headers.Get("Content-Md5"); x != "" {
		buf.WriteString(x)
	} else if len(req.Body) > 0 {
		h := md5.New()
		h.Write(req.Body)

		buf.WriteString(base64.StdEncoding.EncodeToString(h.Sum(nil)))
	}

	buf.WriteByte('\n')

	buf.WriteString(req.Headers.Get("Content-Type"))
	buf.WriteByte('\n')

	buf.WriteString(req.Headers.Get("Date"))
	buf.WriteByte('\n')

	if x := canonicalAmzHeaders(req); x != "" {
		buf.WriteString(x)
	}
	buf.WriteString(canonicalResourceS3(c.cfg.AdminPath, req))

	return buf.Bytes()
}

func hmacSHA1(key []byte, content []byte) []byte {
	mac := hmac.New(sha1.New, key)
	mac.Write(content)

	return mac.Sum(nil)
}

func canonicalResourceS3(pfx string, request *req.Request) string {
	const subresourcesS3 = "acl,lifecycle,location,logging,notification,partNumber,policy,requestPayment,torrent,uploadId,uploads,versionId,versioning,versions,website"

	res := pfx
	uri, _ := url.ParseRequestURI(request.RawURL)

	if isS3VirtualHostedStyle(request) {
		bucketname := strings.Split(uri.Host, ".")[0]
		res += "/" + bucketname
	}

	res += uri.Path

	for _, subres := range strings.Split(subresourcesS3, ",") {
		if strings.HasPrefix(uri.RawQuery, subres) {
			res += "?" + subres
		}
	}

	return res
}

// Info: http://docs.aws.amazon.com/AmazonS3/latest/dev/VirtualHosting.html
func isS3VirtualHostedStyle(request *req.Request) bool {
	uri, _ := url.ParseRequestURI(request.RawURL)
	host := uri.Host
	service, _ := serviceAndRegion(host)
	return service == "s3" && strings.Count(host, ".") == 3
}

// serviceAndRegion parsers a hostname to find out which ones it is.
// http://docs.aws.amazon.com/general/latest/gr/rande.html
func serviceAndRegion(host string) (service string, region string) {
	// These are the defaults if the hostname doesn't suggest something else
	region = "us-east-1"
	service = "s3"

	parts := strings.Split(host, ".")
	if len(parts) == 4 {
		// Either service.region.amazonaws.com or virtual-host.region.amazonaws.com
		if parts[1] == "s3" {
			service = "s3"
		} else if strings.HasPrefix(parts[1], "s3-") {
			region = parts[1][3:]
			service = "s3"
		} else {
			service = parts[0]
			region = parts[1]
		}
	} else if len(parts) == 5 {
		service = parts[2]
		region = parts[1]
	} else {
		// Either service.amazonaws.com or s3-region.amazonaws.com
		if strings.HasPrefix(parts[0], "s3-") {
			region = parts[0][3:]
		} else {
			service = parts[0]
		}
	}

	if region == "external-1" {
		region = "us-east-1"
	}

	return
}

func canonicalAmzHeaders(request *req.Request) string {
	var headers []string

	for header := range request.Headers {
		standardized := strings.ToLower(strings.TrimSpace(header))
		if strings.HasPrefix(standardized, "x-amz") {
			headers = append(headers, standardized)
		}
	}

	sort.Strings(headers)

	for i, header := range headers {
		headers[i] = header + ":" + strings.ReplaceAll(request.Headers.Get(header), "\n", " ")
	}

	if len(headers) > 0 {
		return strings.Join(headers, "\n") + "\n"
	}

	return ""
}

func hashMD5(content []byte) string {
	h := md5.New()
	h.Write(content)
	return base64.StdEncoding.EncodeToString(h.Sum(nil))
}

func (c *Client) BucketStats(ctx context.Context, uid string, bucket string) ([]BucketStatsResponse, error) {
	data := make([]BucketStatsResponse, 0, 5)

	req := c.cli.Get("/bucket").
		SetQueryParam("stats", "true")

	if uid != "" {
		req.SetQueryParam("uid", uid)
	}

	if bucket != "" {
		req.SetQueryParam("bucket", bucket)
	}

	resp := req.
		SetResult(&data).
		Do(ctx)
	if resp.Err != nil {
		return nil, resp.Err
	}

	return data[:], nil
}
